"use strict";
var middleware = require("body-parser");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:bodyparser");
  debug("start");

  debug(".useSetup");
  for (var i=0; i < options.parsers.length; i++){
    var parser = options.parsers[i].parser;
    var parserOptions = options.parsers[i].options;
    imports.express.useSetup(middleware[parser](parserOptions));
  }

  debug("register nothing");
  register(null, {});
};
